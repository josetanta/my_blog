"""Flask testing"""

# Flask
from flask import current_app

from tests.utils import APITestSetup


class BasicTestCase(APITestSetup):

    WITH_USER = False

    def test_should_app_exists(self):
        self.assertFalse(current_app is None)

    def test_should_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])
