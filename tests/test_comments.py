import random

from tests.utils import APITestSetup, json

from app.seeders import (
    generate_posts,
    generate_comments,
    fake,
)


class CommentAPITestCase(APITestSetup):

    def test_should_show_list_comments(self):
        comments = generate_comments()

        response = self.client.get('/api/comments/')
        self.assertEqual(response.status_code, 200)

        data_json = self.parse_data(response)
        self.assertEqual(data_json['count'], len(comments))
        self.assertIsInstance(data_json['data'], list)

    def test_should_show_list_empty_comments(self):
        response = self.client.get('/api/comments/')
        self.assertEqual(response.status_code, 200)

        data_json = self.parse_data(response)
        self.assertEqual(data_json['count'], 0)

    def test_should_create_a_comment(self):
        token = self.response_of_login()

        posts = generate_posts(2, 1)
        post = random.choice(posts)

        response = self.client.post(
            '/api/comments/',
            data=json.dumps({
                'content': fake.text(),
                'post_id': post.id
            }),
            content_type='application/json',
            headers=self.get_api_headers(token)
        )

        self.assertEqual(response.status_code, 201)
