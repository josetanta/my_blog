import unittest
import json

from app import create_app, db
from app.seeders import create_roles, fake
from app.models import (
    User,
    Role,
    Account,
    AnonymousUser
)
from app.utils import get_uuid


class APITestSetup(unittest.TestCase):

    WITH_USER = True
    TYPE_USER = 'User'

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        create_roles()
        self.client = self.app.test_client()

        if self.WITH_USER:
            self.user = self._generate_user()
        else:
            self.user = AnonymousUser()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def parse_data(self, response):
        data = response.get_data(as_text=True)
        return json.loads(data)

    def get_api_headers(self, token):
        return {
            'Authorization': f'Bearer {token}',
            'Accept': 'application/json; multipart/form-data; application/x-www-form-urlencoded',
            'Content-Type': 'application/json; multipart/form-data; application/x-www-form-urlencoded',
        }

    def _generate_user(self):
        self.role = Role.query.filter_by(name=self.TYPE_USER or 'User').first()

        self.password = 'hard-password'

        user = User(
            email=fake.email(),
            username=fake.user_name(),
            password=self.password,
            role=self.role,
            uuid=get_uuid()
        )

        user.account = Account()

        db.session.add(user)
        db.session.commit()

        self.assertIsNotNone(user)
        return user

    def response_of_login(self):
        """
        User login with credentials
        then return a token authenticated
        """
        if self.WITH_USER:
            self.user.confirmed = True

            response = self.client.post(
                '/api/login/',
                data={
                    'email': self.user.email,
                    'password': self.password
                },
            )

            self.assertEqual(response.status_code, 200)
            token = self.parse_data(response)['token']

            return token
        else:
            raise Exception(f'The property WITH_USER is {self.WITH_USER}, please correctly.')
