from tests.utils import APITestSetup


class APITestCase(APITestSetup):

    def test_should_code_is_equal_404(self):
        response = self.client.get('/wrong/url/')
        self.assertEqual(response.status_code, 404)

    def test_anonymous_get_posts_success(self):
        response = self.client.get('/api/posts/')
        self.assertEqual(response.status_code, 200)

    def test_uncofirmed_account(self):
        # add an user
        token = self.user.generate_auth_token()
        self.assertIsNotNone(token)

        response = self.client.post(
            '/api/posts/',
            content_type='multipart/form-data',
            headers=self.get_api_headers(token),
            data={'title': 'Post-test title', 'content': 'Description post Post-test'}
        )

        self.assertEqual(response.status_code, 403)

    def test_user_login(self):
        self.user.confirmed = True

        token = self.user.generate_auth_token()

        self.assertIsNotNone(self.user)
        self.assertEqual(self.user.confirmed, True)

        res = self.client.post(
            '/api/login/',
            data={
                'email': self.user.email,
                'password': self.password
            }
        )

        self.assertEqual(res.status_code, 200)

        res = self.client.post(
            '/api/logout/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(res.status_code, 200)
