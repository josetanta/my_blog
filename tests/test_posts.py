import random
from tests.utils import APITestSetup

from app.seeders import generate_posts, generate_comments, fake


class PostAPITest(APITestSetup):

    def test_should_list_posts(self):
        TOTAL_POSTS = 5
        generate_posts(TOTAL_POSTS, 2)

        response = self.client.get(
            '/api/posts/'
        )

        self.assertEqual(response.status_code, 200)

        data = self.parse_data(response)
        self.assertEqual(data['count'], TOTAL_POSTS)

    def test_should_show_list_empty(self):

        response = self.client.get(
            '/api/posts/'
        )

        self.assertEqual(response.status_code, 200)

        data = self.parse_data(response)
        self.assertEqual(data['count'], 0)

    def test_should_a_single_post(self):
        posts = generate_posts(2, 1)
        post = posts[0]

        response = self.client.get(
            f'/api/posts/{post.uuid}/',
        )

        self.assertEqual(response.status_code, 200)

        data = self.parse_data(response)
        data = data['data']
        relations = data['relationships']
        self.assertEqual(data['attributes']['title'], post.title)
        self.assertEqual(relations['users']['data']['username'], post.author.username)

    def test_should_is_error_not_found_post(self):

        response = self.client.get(
            f'/api/posts/{fake.uuid4()}/',
        )

        self.assertEqual(response.status_code, 404)

    def test_should_create_a_post(self):

        # Begin - user authenticated and confirmed
        # account
        token = self.response_of_login()

        data = {
            'title': 'Post-new-test',
            'content': 'Description a new post'
        }

        response = self.client.post(
            '/api/posts/',
            data=data,
            content_type='application/x-www-form-urlencoded',
            headers=self.get_api_headers(token),
        )
        self.assertEqual(response.status_code, 201)

        data_json = self.parse_data(response)
        attrs = data_json['data']['attributes']
        self.assertIsNotNone(data_json)
        self.assertEqual(attrs['content'], data['content'])

    def test_should_error_create_post(self):
        # Begin - user authenticated and confirmed
        # account
        token = self.response_of_login()

        data = {
            'title': '',
            'content': ''
        }

        response = self.client.post(
            '/api/posts/',
            data=data,
            content_type='multipart/form-data',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 400)

    def test_should_list_comments_by_post(self):
        comments = generate_comments()

        comment = random.choice(comments)
        post = comment.post

        response = self.client.get(f'/api/posts/{post.uuid}/comments/')
        self.assertEqual(response.status_code, 200)

        # Verify all post self comments
        data_json = self.parse_data(response)
        self.assertEqual(data_json['count'], len(post.comments))
