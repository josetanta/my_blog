# App
from app import db

# Utilities
from tests.utils import APITestSetup

# Models
from app.models import User

# Seeders
from app.seeders import fake


class UserFollowTestCase(APITestSetup):

    def test_should_follow_an_user(self):
        # Login user
        token = self.response_of_login()

        followed = User(
            confirmed=True,
            email=fake.email(),
            username=fake.user_name(),
            uuid=fake.uuid4(),
            password='fake-user',
            password_hash='fake-user',
        )
        db.session.add(followed)
        db.session.commit()
        self.assertIsNotNone(followed.id)
        self.assertTrue(followed.confirmed)

        response = self.client.post(
            f'/api/users/{followed.id}/follow/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 200)

    def test_should_show_followers_of_an_user(self):
        # Login user
        token = self.response_of_login()

        followed = User(
            confirmed=True,
            email=fake.email(),
            username=fake.user_name(),
            uuid=fake.uuid4(),
            password='fake-user',
            password_hash='fake-user',
        )
        db.session.add(followed)
        db.session.commit()

        response = self.client.post(
            f'/api/users/{followed.id}/follow/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f'/api/followers/{followed.uuid}/',
        )
        data_json = self.parse_data(response)
        followers = data_json['data']
        self.assertEqual(len(followers), 1)

        self.assertTrue(self.user.email == followers[0]['email'])

    def test_should_show_the_following(self):

        follower = User(
            confirmed=True,
            email=fake.email(),
            username=fake.user_name(),
            uuid=fake.uuid4(),
            password='fake-user',
        )
        db.session.add(follower)
        db.session.commit()

        response = self.client.post(
            '/api/login/',
            data={
                'email': follower.email,
                'password': 'fake-user'
            }
        )
        self.assertEqual(response.status_code, 200)
        data_json = self.parse_data(response)
        self.assertIsNotNone(data_json['token'])

        token_follower = data_json['token']
        response = self.client.post(
            f'/api/users/{self.user.id}/follow/',
            headers=self.get_api_headers(token_follower)
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f'/api/followed/{follower.uuid}/',
        )
        data_json = self.parse_data(response)
        following = data_json['data'][0]
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(following)
        self.assertEqual(following['email'], self.user.email)

    def test_should_unfollow_user_of_followed(self):
        # Login user
        token = self.response_of_login()

        followed = User(
            confirmed=True,
            email=fake.email(),
            username=fake.user_name(),
            uuid=fake.uuid4(),
            password='fake-user',
            password_hash='fake-user',
        )
        db.session.add(followed)
        db.session.commit()

        response = self.client.post(
            f'/api/users/{followed.id}/follow/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            f'/api/users/{followed.id}/unfollow/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 200)

    def test_should_verify_a_follower(self):
        token = self.response_of_login()
        followed = User(
            confirmed=True,
            email=fake.email(),
            username=fake.user_name(),
            uuid=fake.uuid4(),
            password='fake-user',
            password_hash='fake-user',
        )
        db.session.add(followed)
        db.session.commit()

        # first request
        response = self.client.post(
            f'/api/users/{followed.id}/follow/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 200)

        # second request
        response = self.client.post(
            f'/api/users/{followed.id}/follow/',
            headers=self.get_api_headers(token)
        )
        self.assertEqual(response.status_code, 400)
