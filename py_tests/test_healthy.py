"""Testing of health"""
from flask.testing import FlaskClient


def it_request_is_ok(client: FlaskClient):

    response = client.get('/api/healthy/')

    assert response.status_code == 200
