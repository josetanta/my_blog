"""Config testing client"""

# pytest
import pytest

# Flask
from flask import Flask

# Application
from app import create_app, db
from app.seeders import create_roles


@pytest.fixture()
def app():
    app = create_app('testing')
    app_context = app.app_context()
    app_context.push()
    db.create_all()
    create_roles()

    yield app

    db.session.remove()
    db.drop_all()
    app_context.pop()


@pytest.fixture()
def client(app: Flask):
    return app.test_client()


@pytest.fixture()
def runner(app: Flask):
    return app.test_cli_runner()
