from datetime import datetime

from flask import url_for

from app import db
from app.utils import _generate_slug


class Follow(db.Model):
    __tablename__ = 'follows'
    followed_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), primary_key=True)
    follower_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.now)


class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    title = db.Column(db.String(120), nullable=False)
    uuid = db.Column(db.String(400), nullable=False, unique=True)
    content = db.Column(db.Text, nullable=False)
    slug = db.Column(db.String(100), nullable=False, index=True)
    publishied = db.Column(db.Boolean, default=True, index=True)
    image = db.Column(db.String(200), nullable=True, default='image.jpg')
    url_image = db.Column(db.String(600), nullable=True, default='')
    deleted = db.Column(db.Boolean, default=False)
    date_created = db.Column(
        db.DateTime,
        default=datetime.utcnow,
        nullable=False
    )

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True)
    comments = db.relationship(
        'Comment', backref='post', lazy=True, cascade="all, delete-orphan")

    def __setattr__(self, key, value):
        super(Post, self).__setattr__(key, value)
        if key == 'title':
            self.slug = _generate_slug(self.title)

    @property
    def image_path(self):
        if self.image in 'image.jpg':
            return url_for('static', filename='uploads/image.jpg', _external=True)
        else:
            return url_for('static', filename=f'uploads/posts/{self.image}', _external=True)

    def to_json(self):
        return {
            'type': 'posts',
            'id': self.id,
            'slug': self.slug,
            'uuid': self.uuid,
            'attributes': {
                'title': self.title,
                'content': self.content,
                'publishied': self.publishied,
                'image': self.image_path if self.image_path.endswith(('.jpg', '.png')) else '',
                'created': self.date_created,
            },
            'links': {
                'self': url_for('api.post_api', post_id=self.id, _external=True),
            },
            'relationships': {
                'users': {
                    'data': {
                        'type': 'users',
                        'id': str(self.author.id),
                        'uuid': self.author.uuid,
                        'username': self.author.username,
                        'image': self.author.image_path
                    },
                    'links': {
                        'related': url_for('api.user_api', user_id=self.user_id, _external=True),
                        'self': url_for('api.user_post', user_id=self.author.id, post_id=self.id, _external=True)
                    }
                },
                'comments': {
                    'data': [{'type': 'comments', 'id': str(comment.id)} for comment in self.comments],
                    'links': {
                        'related': url_for('api.post_comments', post_id=self.id, _external=True)
                    }
                }
            }

        }

    def get_id(self):
        obj = dict({
            'id': self.id,
            'publishied': self.publishied,
        })
        return obj

    @property
    def is_deleted(self):
        return self.deleted

    def __repr__(self):
        return f"<Post {self.title} {self.date_created}>"
