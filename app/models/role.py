from app import db


class Permission:
    FOLLOW = 0x01
    COMMENT = 0x02
    WRITE_POST = 0x04
    MODERATE_COMMENTS = 0x08
    ACCESS_API = 0x32
    ADMINISTER = 0xff


class Role(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True, index=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='joined')

    @staticmethod
    def generate_roles():
        roles = {
            'User': (True,
                     Permission.FOLLOW
                     | Permission.COMMENT
                     | Permission.WRITE_POST
                     | Permission.ACCESS_API),
            'Moderator': (False,
                          Permission.FOLLOW
                          | Permission.COMMENT
                          | Permission.WRITE_POST
                          | Permission.MODERATE_COMMENTS
                          | Permission.ACCESS_API),
            'Admin': (False, 0xff),
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.default = roles[r][0]
            role.permissions = roles[r][1]
            db.session.add(role)
        db.session.commit()

    def has_permission(self, permissions):
        return self.permissions & permissions == permissions

    def __repr__(self):
        return "<Role %s>" % self.name
