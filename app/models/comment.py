from datetime import datetime

from flask import url_for


from app import db


class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text, nullable=False)
    publishied = db.Column(db.Boolean, default=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    deleted = db.Column(db.Boolean, default=False)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True)
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'), index=True)

    @property
    def is_deleted(self):
        return self.deleted

    def to_json(self):
        return {
            'type': 'comments',
            'id': str(self.id),
            'attributes': {
                    'content': self.content,
                'created': self.date_created,
            },
            'links': {
                'self': url_for('api.comment_api', comment_id=self.id, _external=True),
            },
            'relationships': {
                'users': {
                    'data': {
                        'type': 'users',
                        'id': str(self.author.id),
                        'uuid': self.author.uuid,
                        'username': self.author.username,
                        'image': self.author.image_path
                    },
                    'links': {
                        'related': url_for('api.user_api', user_id=self.user_id, _external=True),
                        'self': url_for('api.user_comment', comment_id=self.id, user_id=self.user_id,
                                        _external=True)
                    }
                },
                'posts': {
                    'data': {
                        'type': 'posts',
                        'id': str(self.post.id),
                        'uuid': self.post.uuid
                    },
                    'links': {
                        'related': url_for('api.post_api', post_id=self.post_id, _external=True),
                        'self': url_for('api.post_comment', comment_id=self.id, post_id=self.post_id,
                                        _external=True),
                    }
                }
            }
        }

    def get_id(self):
        obj = dict({
            'id': self.id,
            'publishied': self.publishied,
        })
        return obj

    def __repr__(self):
        return '<Comment %d %s>' % (self.id, self.publishied)
