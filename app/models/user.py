from datetime import datetime

from flask_login import UserMixin, AnonymousUserMixin
from flask import current_app, url_for
from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer,
    SignatureExpired,
    BadSignature
)
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from app.models.post import Follow
from app.models.role import Permission, Role
from app.utils import _generate_slug


class Account(db.Model):
    __tablename__ = 'accounts'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    info = db.Column(db.Text, nullable=True)
    name = db.Column(db.String(60), nullable=True)
    date_created = db.Column(db.DateTime, default=datetime.now, nullable=False)
    image = db.Column(db.String(200), default='user.jpg', nullable=True)

    user_id = db.Column(
        db.Integer,
        db.ForeignKey('users.id'),
        index=True,
        nullable=False
    )

    def image_path(self):

        if self.image in 'user.jpg':
            return url_for('static', filename='uploads/avatar.svg', _external=True)
        else:
            return url_for('static', filename=f'uploads/users/{self.image}', _external=True)

    def __repr__(self):
        return f'Account UserID {self.user_id}'


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    username = db.Column(
        db.String(40),
        nullable=False,
        unique=True,
        index=True
    )
    email = db.Column(db.String(100), nullable=False, unique=True, index=True)
    uuid = db.Column(db.String(400), nullable=False, unique=True)
    password_hash = db.Column(db.String(150), nullable=False)
    slug = db.Column(db.String(100), nullable=False, index=True)
    status = db.Column(db.Boolean, default=True, index=True)
    confirmed = db.Column(db.Boolean, default=False)
    suspended = db.Column(db.Boolean, default=False)

    role_id = db.Column(
        db.Integer,
        db.ForeignKey('roles.id'),
        index=True,
        nullable=False
    )

    # One to One
    account = db.relationship(
        'Account',
        backref='user',
        lazy='joined',
        uselist=False,
        cascade='all'
    )

    token = db.relationship(
        'UserToken',
        backref='user',
        lazy='joined',
        uselist=False,
        cascade='all, delete-orphan'
    )

    # One to Many
    posts = db.relationship(
        'Post',
        backref='author',
        lazy=True,
        cascade='all, delete-orphan'
    )

    comments = db.relationship(
        'Comment',
        backref='author',
        lazy=True,
        cascade='all, delete-orphan'
    )

    followed = db.relationship(
        'Follow',
        foreign_keys=[Follow.follower_id],
        backref=db.backref('follower', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )

    followers = db.relationship(
        'Follow',
        foreign_keys=[Follow.followed_id],
        backref=db.backref('followed', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['EMAIL_BLOG_ADMIN']:
                self.role = Role.query \
                    .filter_by(permissions=Permission.ADMINISTER) \
                    .first()
                self.confirmed = True
            if self.role is None:
                self.role = Role.query \
                    .filter_by(default=True) \
                    .first()

    def __setattr__(self, key, value):
        super(User, self).__setattr__(key, value)
        if key == 'username':
            self.slug = _generate_slug(self.username)

    def __repr__(self):
        return f"<User {self.email}>"

    def get_reset_token(self, expires_sec=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        token = s.dumps({'user_id': self.id}).decode('utf-8')
        return token

    @staticmethod
    def verified_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except TypeError:
            return None

        return User.query.get(user_id)

    def to_json(self):
        return dict({
            'id': self.id,
            'uuid': self.uuid,
            'username': self.username,
            'name': self.account.name or '',
            'status': self.status,
            'email': self.email,
            'image': self.image_path,
            'role': self.role.name,
            'confirmed': self.confirmed,
            'followers': str(self.followers.count()),
            'followed': str(self.followed.count()),
            'info': self.account.info
        })

    @property
    def image_path(self):
        return self.account.image_path() if self.account else ''

    def api_to_json(self):
        return {
            'type': self.__tablename__,
            'id': str(self.id),
            'uuid': self.uuid,
            'attributes': {
                'username': self.username,
                'status': self.status,
                'email': self.email,
                'confirmed': self.confirmed,
                'image': self.image_path,
                'name': self.account.name if self.account.name else ''
            },
            'links': {
                'self': url_for('api.user_api', user_id=self.id, _external=True),
            },
            'relationships': {
                'posts': {
                    'data': [{'type': 'posts', 'id': str(post.id), 'uuid': post.uuid} for post in self.posts],
                    'links': {
                        'related': url_for('api.user_posts', user_id=self.id, _external=True)
                    }
                },
                'comments': {
                    'data': [{'type': 'comments', 'id': str(comment.id)} for comment in self.comments],
                    'links': {
                        'related': url_for('api.user_comments', user_id=self.id, _external=True)
                    }
                },
            }
        }

    def follow(self, user):
        if not self.is_following(user):
            f = Follow(follower=self, followed=user)
            db.session.add(f)
            db.session.commit()

    def unfollow(self, user):
        f = self.followed.filter_by(followed_id=user.id).first()
        if f:
            db.session.delete(f)
            db.session.commit()

    def is_following(self, user):
        return self.followed.filter_by(followed_id=user.id).first()

    def is_followed_by(self, user):
        return self.followers.filter_by(follower_id=user.id).first()

    def can(self, permissions):
        return self.role is not None and self.role.has_permission(permissions)

    def follow_to_json(self):
        return dict({
            'id': self.id,
            'uuid': self.uuid,
            'username': self.username,
            'email': self.email,
            'image': self.image_path,
        })

    @property
    def is_administrator(self):
        return self.can(Permission.ADMINISTER)

    def generate_confirmed_token(self, expiration=3600):
        """
        :expiration in 1 hour
        """
        s = Serializer(current_app.config.get('SECRET_KEY'), expiration)
        token = s.dumps({'confirm': self.id}).decode('utf-8')
        return token

    def confirm(self, token):
        s = Serializer(current_app.config.get('SECRET_KEY'))
        try:
            data = s.loads(token.encode('utf-8'))
        except SignatureExpired:
            return False

        if data.get('confirm') != self.id:
            return False

        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    @property
    def password(self):
        raise AttributeError("Don't read password")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    @property
    def followers_list(self):
        return db.session.query(User) \
            .select_from(Follow) \
            .filter_by(followed_id=self.id) \
            .join(User, Follow.follower_id == User.id).all()

    @property
    def followed_list(self):
        return db.session.query(User) \
            .select_from(Follow) \
            .filter_by(follower_id=self.id) \
            .join(User, Follow.followed_id == User.id).all()

    def generate_auth_token(self, expires=2 * 24 * 60 * 60):
        s = Serializer(current_app.config.get(
            'SECRET_KEY'), expires_in=expires)
        token = s.dumps({'user_id': self.id}).decode(encoding='utf-8')
        if not self.has_token():
            tk = UserToken(token_hash=token, user=self)
            db.session.add(tk)
            db.session.commit()
            return tk.get_token()
        else:
            return self.get_token()

    @staticmethod
    def verify_auth_token(token: str):

        s = Serializer(current_app.config.get('SECRET_KEY'))
        try:
            data = s.loads(token)
        except BadSignature:
            return None

        except SignatureExpired:
            return None

        if UserToken.exist_token(data.get('user_id')):
            return User.query.get(data.get('user_id'))
        return None

    def get_token(self):
        return self.token.get_token()

    def has_token(self):
        return bool(self.token)

    @property
    def is_suspended(self):
        return self.suspended


class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False

    def is_administrator(self):
        return False


class UserToken(db.Model):
    __tablename__ = 'user_tokens'
    id = db.Column(db.Integer, primary_key=True)
    token_hash = db.Column(db.String(600), unique=True, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime)

    @staticmethod
    def delete_token(user_id):

        t = UserToken.query.filter_by(user_id=user_id).first()

        if t:
            db.session.delete(t)
            db.session.commit()
            return True
        else:
            return False

    def get_token(self):
        return self.token_hash

    @staticmethod
    def exist_token(user_id) -> bool:
        t = UserToken.query.filter_by(user_id=user_id).first()
        if t:
            return True
        return False

    def __repr__(self) -> str:
        return f"<Token {self.id}>"
