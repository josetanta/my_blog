from functools import wraps

from flask_httpauth import request
from werkzeug.datastructures import CombinedMultiDict

from app.models import AnonymousUser

# Auth Api
from .authentication import token_auth

from .handlers import forbidden, unathorized, bad_request

CONTENT_TYPE = ['application/json']


def permission_required(permission):
    def wrapped(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if isinstance(token_auth.current_user(), AnonymousUser):
                return unathorized('Unauthorized user')

            if not token_auth.current_user().confirmed:
                return forbidden("Unconfirmed account")

            if not token_auth.current_user().can(permission):
                return forbidden("Insufficient permissions")

            return f(*args, **kwargs)

        return decorated

    return wrapped


def check_user(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        if token_auth.current_user().is_anonymous:
            return unathorized("Not Access")

        if kwargs.get('user_id'):
            if str(token_auth.current_user().id) != str(kwargs.get('user_id')):
                return unathorized("Not Access")

        if kwargs.get('slug'):
            if str(token_auth.current_user().slug) != str(kwargs.get('slug')):
                return unathorized("Not Access")

        if kwargs.get('uuid'):
            if str(token_auth.current_user().uuid) != str(kwargs.get('uuid')):
                return unathorized("Not Access")

        return f(*args, **kwargs)

    return decorated


def form_valid_request(form_class):
    def wrapped(f):

        @wraps(f)
        def decorated(*args, **kwargs):
            if bool(request.form) or bool(request.files):

                form = form_class(CombinedMultiDict((request.form, request.files)))
                resp = {
                    'errors': []
                }

                if form.validate():
                    return f(*args, **kwargs)
                else:
                    for field, err in form.errors.items():
                        tmp = {
                            'field': field,
                            'info': getattr(form, field).label.text,
                            'message': err
                        }
                        resp['errors'].append(tmp)
                    return bad_request(None, **resp)

            elif request.content_type in CONTENT_TYPE and bool(request.data):
                return f(*args, **kwargs)

            else:
                return bad_request('Content type no supported.')

        return decorated

    return wrapped
