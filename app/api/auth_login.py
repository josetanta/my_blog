from flask_httpauth import request, current_app

from app.models import User, UserToken

from app.api import api
from .authentication import token_auth
from .decorators import form_valid_request
from .forms import LoginFormAPI
from .handlers import bad_request, internal_server, get_ok, unathorized


@api.post('/login/')
@form_valid_request(LoginFormAPI)
def login_auth():
    try:
        form = LoginFormAPI(request.form)
        user = User.query.filter_by(email=form.email.data).first()
        if not user:
            return bad_request('Este usuario no se encuentra registrado')

        elif form.validate_on_submit():
            if user.verify_password(form.password.data):
                token = user.generate_auth_token(expires=current_app.config.get('MAX_AGE_TOKEN'))

                return get_ok('Inicio de sesión con exito', token=token)
            else:
                return bad_request('Las credenciales del usuario son invalidas')
        else:
            return bad_request('Las credenciales del usuario son invalidas')

    except TypeError:
        return internal_server('Error de servidor')


@token_auth.login_required
@api.get('/user-auth/')
def get_user():
    try:
        return get_ok('Retrieve User', user=token_auth.current_user().to_json())
    except BaseException:
        return unathorized('No Authenticated', user=None)


@token_auth.login_required
@api.post('/logout/')
def logout_auth():
    user_id = token_auth.current_user().id
    if user_id and UserToken.delete_token(user_id):
        return get_ok('Cerro sesión con exito', user=None)
    else:
        return bad_request('No se encontro al usuario')


@api.get('/healthy/')
def healthy():
    return get_ok('Health', status='ok')
