from flask import Blueprint

api = Blueprint('api', __name__, url_prefix='/api')

from . import auth_login, comments, posts, users, follows, handlers, decorators, confirm_account, forms, utils  # noqa
