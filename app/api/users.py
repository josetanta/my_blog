import asyncio

from flask import url_for
from flask.views import MethodView
from flask_httpauth import request, current_app
from sqlalchemy.util import NoneType
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import BadRequestKeyError

from app.mail import send_token_confirmation_client
from app import db
from app.models import User, Post, Comment, Account
from app.utils import save_upload, get_uuid, delete_upload

from app.api import api
from .authentication import token_auth
from .decorators import check_user, form_valid_request
from .forms import RegisterFormAPI, AccountFormAPI
from .handlers import not_found, bad_request, get_ok
from .utils import (
    register_api,
)


class UserAPIMethodView(MethodView):

    def get(self, user_id):
        try:
            if user_id:
                user = User.query.get(user_id)
                if user:
                    return get_ok('OK', data=user.api_to_json())
                else:
                    return bad_request('El usuario no se encuentra registrado')
            else:
                return get_ok('OK', data=[user.api_to_json() for user in User.query.all()])
        except NoneType:

            return not_found('Usuario no encontrado')

    @form_valid_request(RegisterFormAPI)
    def post(self):
        '''
        Register API
        '''
        form = RegisterFormAPI(request.form)

        if form.validate_on_submit():
            user = User(
                username=form.username.data,
                email=form.email.data,
                password=form.password.data,
                uuid=get_uuid(),
            )

            user.account = Account()

            token_confirmed = user.generate_confirmed_token()
            origin = request.headers.get('Origin', '')
            asyncio.run(
                send_token_confirmation_client(
                    user,
                    token_confirmed,
                    url_client=origin
                )
            )

            db.session.add(user)
            db.session.commit()

            token_auth = user.generate_auth_token(expires=current_app.config.get('MAX_AGE_TOKEN'))
            return get_ok('Su cuenta ah sido creado', data=user.to_json(), token=token_auth)

        else:
            return bad_request('Error en la petición', data=None)

    @check_user
    @token_auth.login_required
    def delete(self, user_id):
        user = User.query.get(user_id)
        if user:
            db.session.delete(user)
            db.session.commit()

            return get_ok('Usuario Eliminado')

        return not_found('Usuario no encontrado')

    @check_user
    @token_auth.login_required
    @form_valid_request(AccountFormAPI)
    def put(self, user_id):
        try:

            form = AccountFormAPI(CombinedMultiDict(
                (request.form, request.files)))

            user = User.query.get(user_id)
            if user and form.validate_on_submit():

                user.account.name = form.name.data
                user.account.info = form.info.data

                user.username = form.username.data
                user.email = form.email.data

                try:
                    if form.image.data:
                        img = save_upload(
                            form.image.data,
                            directory='users',
                            obj=user.account,
                            size=1200
                        )
                        user.account.image = img
                    else:
                        pass
                except AttributeError:
                    pass

                db.session.commit()

                return get_ok('Su cuenta fue actualizado', data=user.to_json())

            else:
                return bad_request('Error de servidor')
        except BadRequestKeyError:
            return bad_request('Error de servidor')


register_api(UserAPIMethodView, 'user_api',
             '/users/', pk='user_id', pk_type='int')


@check_user
@api.get("/users/<uuid>/")
def user_uuid(uuid):

    user = User.query.filter_by(uuid=uuid).first()

    if user:
        return get_ok('OK', data=user.api_to_json())
    else:
        return not_found('El usuario no ha sido encontrado')


@api.get('/users/<int:user_id>/posts/')
def user_posts(user_id: int):
    page = request.args.get('page', 1, type=int)
    pagination = Post.query \
        .filter_by(user_id=user_id) \
        .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)
    posts = pagination.items

    prev_page = None
    if prev_page:
        prev_page = url_for('api.user_posts', user_id=user_id,
                            page=page - 1, _external=True)

    next_page = None
    if next_page:
        next_page = url_for('api.user_posts', user_id=user_id,
                            page=page + 1, _external=True)

    return get_ok('OK',
                  data=[post.to_json() for post in posts],
                  links={'prev_page': prev_page, 'next_page': next_page},
                  count=pagination.total
                  )


@api.get('/users/<uuid>/posts/')
def user_posts_slug(uuid):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(uuid=uuid).first()
    pagination = Post.query \
        .filter_by(user_id=user.id) \
        .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)
    posts = pagination.items

    prev_page = None
    if prev_page:
        prev_page = url_for(
            'api.user_posts_slug',
            uuid=uuid,
            page=page - 1,
            _external=True
        )

    next_page = None
    if next_page:
        next_page = url_for(
            'api.user_posts_slug',
            uuid=uuid,
            page=page + 1,
            _external=True
        )

    return get_ok(
        'ok',
        data=[post.to_json() for post in posts],
        links={'prev_page': prev_page, 'next_page': next_page},
        count=pagination.total
    )


@api.get('/users/<int:user_id>/posts/')
def user_post(user_id: int):
    post = Post.query.filter_by(user_id=user_id).first()

    return get_ok('OK', data=post.to_json())


@api.get('/users/<int:user_id>/comments/')
def user_comments(user_id: int):
    page = request.args.get('page', 1, type=int)
    pagination = Comment.query \
        .filter_by(user_id=user_id) \
        .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)

    comments = pagination.items

    prev_page = None
    if prev_page:
        prev_page = url_for('api.user_comments', user_id=user_id,
                            page=page - 1, _external=True)

    next_page = None
    if next_page:
        next_page = url_for('api.user_comments', user_id=user_id,
                            page=page + 1, _external=True)
    return get_ok('OK',
                  data=[comment.to_json() for comment in comments],
                  links={'prev_page': prev_page, 'next_page': next_page},
                  count=pagination.total
                  )


@api.get('/users/<int:user_id>/comments/')
def user_comment(user_id=None):
    comment = Comment.query.filter_by(user_id=user_id).first()

    return get_ok('OK', data=comment.to_json())


@api.get('/users/<int:user_id>/account/')
@check_user
@token_auth.login_required
def user_account(user_id):
    user = User.query.get(user_id)
    if user:
        return get_ok(f'Cuenta del usuario {user.email}', data=user.to_json())

    else:
        return not_found('Usuario no encontrado')

# TODO: Delete image


@token_auth.login_required
@api.delete('/remove-image/')
def remove_image():
    image_deleted = token_auth.current_user().account.image
    if not (image_deleted == 'user.jpg'):
        delete_upload(token_auth.current_user().account, 'users')
        token_auth.current_user().account.image = 'user.jpg'

    db.session.commit()
    return get_ok('Su imagen fue eliminado.', user=token_auth.current_user().to_json())
