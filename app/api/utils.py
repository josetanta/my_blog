from app.api import api


def register_api(view, endpoint, url, pk='id', pk_type=''):
    view_func = view.as_view(endpoint)

    api.add_url_rule(
        url,
        defaults={pk: None},
        view_func=view_func,
        methods=['GET']
    )

    api.add_url_rule(
        url,
        view_func=view_func,
        methods=['POST']
    )

    api.add_url_rule(
        '%s<%s:%s>/' % (url, pk_type, pk),
        view_func=view_func,
        methods=['GET', 'PUT', 'DELETE', 'PATCH']
    )


def get_object_or_404():
    pass
