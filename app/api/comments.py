from flask import (
    jsonify,
    url_for,
)
from flask_httpauth import (
    request,
    make_response,
    current_app,
)
from flask.views import MethodView

from app import db
from app.models import Comment, Permission

from .authentication import token_auth
from .handlers import not_found, get_ok, forbidden, create_ok
from .decorators import permission_required, form_valid_request
from .forms import CommentFormAPI
from .utils import register_api


class CommentAPIMethodView(MethodView):

    def get(self, comment_id: int):

        if comment_id is None:
            page = request.args.get('page', 1, type=int)
            pagination = Comment.query \
                .order_by(Comment.date_created.desc()) \
                .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)

            comments = pagination.items
            prev_page = None
            if pagination.has_prev:
                prev_page = url_for(
                    'api.comment_api', page=page - 1, _external=True)
            next_page = None
            if pagination.has_next:
                next_page = url_for(
                    'api.comment_api', page=page + 1, _external=True)

            res = make_response(
                jsonify(
                    {
                        'data': [comment.to_json() for comment in comments],
                        'links': {'next_page': next_page, 'prev_page': prev_page,
                                  'self': url_for('api.comment_api', _external=True)},
                        'count': pagination.total
                    })
            )

            res.set_cookie('PAGE_COMMENT', str(page))
            return res, 200
        else:
            comment = Comment.query.get(comment_id).to_json() \
                if Comment.query.get(comment_id) else None

            return make_response({'data': comment}), 200

    @token_auth.login_required
    @permission_required(Permission.COMMENT)
    def post(self):
        data_json = request.get_json()
        comment = Comment(
            content=data_json['content'],
            post_id=data_json['post_id']
        )
        comment.author = token_auth.current_user()

        db.session.add(comment)
        db.session.commit()

        return create_ok('Comentario Creado', data=comment.to_json())

    @token_auth.login_required
    @permission_required(Permission.COMMENT)
    def delete(self, comment_id: int):
        comment = Comment.query.get(comment_id)

        if token_auth.current_user().id == comment.author.id or comment:
            db.session.delete(comment)
            db.session.commit()
            return get_ok('Comentario eliminado')

        return not_found('Comentario no encontradro')

    @token_auth.login_required
    @permission_required(Permission.COMMENT)
    @form_valid_request(CommentFormAPI)
    def put(self, comment_id: int):

        comment = Comment.query.get(comment_id)
        form = CommentFormAPI()

        if token_auth.current_user().id != comment.author.id:
            return forbidden('Usted no tiene permiso para esta accion')

        if str(comment.post_id) != form.post_id.data:
            return forbidden('Usted no tiene permiso para esta accion')

        if form.validate_on_submit():
            comment.content = form.content.data
            db.session.commit()
            return get_ok('Comentario Actualizado', data=comment.to_json())

        return not_found('No se encontro al comentario')


register_api(CommentAPIMethodView, 'comment_api', '/comments/', pk='comment_id', pk_type='int')
