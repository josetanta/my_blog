from typing import Any, Type
from flask_httpauth import HTTPTokenAuth

from app.models import User, AnonymousUser
from app.api import api
from .handlers import unathorized, forbidden, get_ok

token_auth = HTTPTokenAuth()


@token_auth.verify_token
def verify_token(token):
    anonymous = AnonymousUser()
    if not bool(token):
        return anonymous

    user = User.verify_auth_token(token)

    if not bool(user):
        return anonymous

    return user


@api.before_request
@token_auth.login_required
def before_request():
    # TODO: complete
    u = token_auth.current_user() or AnonymousUser()
    if not u.is_anonymous and not u.confirmed:
        return forbidden('Unconfirmed account')


@token_auth.error_handler
def auth_error():
    return unathorized("Invalid credentials")


@api.get("/refresh-token/")
def get_token():
    current_user: Type[User | Any] = token_auth.current_user()
    if current_user.is_anonymous:
        return unathorized('Unauthorized')
    else:
        return get_ok('ok', token=current_user.generate_auth_token())
