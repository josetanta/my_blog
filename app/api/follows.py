from app.models import User

from app.api import api
from .authentication import token_auth
from .handlers import get_ok, bad_request, not_found


@api.get('/followers/<uuid>/')
def get_followers(uuid):
    user = User.query.filter_by(uuid=uuid).first()

    if user:
        return get_ok('OK', data=[f.follow_to_json() for f in user.followers_list], count=user.followers.count())

    return not_found('No se encontro al usuario')


@api.get('/followed/<uuid>/')
def get_followed(uuid):
    user = User.query.filter_by(uuid=uuid).first()

    if user:
        return get_ok('OK', data=[f.follow_to_json() for f in user.followed_list], count=user.followed.count())

    return not_found('No se encontro al usuario')


@token_auth.login_required
@api.post('/users/<int:user_id>/follow/')
def user_follow(user_id):
    user = User.query.get_or_404(user_id)

    try:
        if token_auth.current_user().is_following(user):
            return bad_request(f'Ya esta siguiendo a este usuario {user.email}')

        else:
            token_auth.current_user().follow(user)
            return get_ok(f'Usted empezo a seguir a {user.email}')

    except AttributeError:

        return not_found('No se encontro al usuario')


@token_auth.login_required
@api.post('/users/<int:user_id>/unfollow/')
def user_unfollow(user_id):
    following = User.query.get_or_404(user_id)
    token_auth.current_user().unfollow(following)
    return get_ok(f'Usted dejo de seguir a {following.email}')
