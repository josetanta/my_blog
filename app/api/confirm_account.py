import asyncio

from flask_httpauth import request

from app.models import User
from app.mail import send_token_confirmation_client
from app.api import api
from .authentication import token_auth
from .handlers import bad_request, get_ok


@api.post('/confirm-account/<token>/')
@token_auth.login_required
def confirm_account(token):
    if token_auth.current_user().confirm(token):
        return get_ok('Usted ah confirmado su cuenta', data=token_auth.current_user().to_json())

    return bad_request('Ah expirado el tiempo de autenticación de su cuenta')


@api.post('/resend-token/<int:user_id>/')
@token_auth.login_required
def resend_token_confirmed(user_id: int):
    user = User.query.get(user_id)

    if user:
        token = user.generate_confirmed_token()
        origins = request.headers.get('Origin', '')
        asyncio.run(
            send_token_confirmation_client(
                user,
                token,
                url_client=origins
            )
        )

        return get_ok('Se le reenvio un mensage a su email')

    return bad_request('Este usuario no se encuentra registrado')
