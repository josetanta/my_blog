from flask_httpauth import current_app
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import PasswordField, StringField, TextAreaField
from wtforms.validators import Email, DataRequired, Length, ValidationError, EqualTo

# Auth API
from .authentication import token_auth
from app.models import Post, User

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}


class LoginFormAPI(FlaskForm):
    email = StringField(validators=[
        DataRequired("Por favor vuelva a ingresar su email."),
        Email('Por favor Ingrese un email Correcto.')
    ])

    password = PasswordField(validators=[
        DataRequired("Por favor intente ingresar una contraseña correcta.")
    ])


class RegisterFormAPI(FlaskForm):
    username = StringField(validators=[
        DataRequired("Este Campo es requerido."),
        Length(4, 40)
    ])
    email = StringField(validators=[
        DataRequired("Este Campo es requerido."),
        Email("Este Email no es valido."),
        Length(min=10, message="El Email como minimo debera tener 10 caracteres.")
    ])
    password = PasswordField(validators=[
        DataRequired("Este Campo es requerido."),
        EqualTo('password_repeat', "La contraseña ingresada no es correcta.")
    ])
    password_repeat = PasswordField(validators=[
        DataRequired("Este Campo es requerido."),
        EqualTo('password', "Por favor intente ingresar nuevamente la contraseña.")
    ])

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data.strip()).first()
        if user:
            raise ValidationError('Este nombre de usuario ya existe.')

    def validate_email(self, field):
        email = User.query.filter_by(email=field.data).first()
        if email or (email == current_app.config.get('EMAIL_BLOG_ADMIN')):
            raise ValidationError('Este email ya existe.')


class AccountFormAPI(FlaskForm):
    email = StringField(validators=[Email("Este email es incorrecto.")])
    username = StringField(validators=[Length(4, 40)])
    name = StringField(validators=[Length(0, 60)])
    info = TextAreaField()
    image = FileField(validators=[
        FileAllowed(ALLOWED_EXTENSIONS),
    ])

    def validate_username(self, username):
        if username.data != token_auth.current_user().username:
            user = User.query.filter_by(username=username.data.strip()).first()
            if user:
                raise ValidationError('Este nombre de usuario ya existe.')

    def validate_email(self, email):
        if email.data != token_auth.current_user().email:
            user = User.query.filter_by(email=email.data.strip()).first()
            if user:
                raise ValidationError('Este email es incorrecto.')


class PostFormAPI(FlaskForm):
    title = StringField(validators=[DataRequired(), Length(6, 120)])
    content = TextAreaField(validators=[DataRequired()])
    image = FileField(validators=[
        FileAllowed(ALLOWED_EXTENSIONS),
    ])


class CommentFormAPI(FlaskForm):
    content = TextAreaField(validators=[
        Length(min=20, message='Por favor el comentario debe tener más de 20 caracteres.')]
    )
    post_id = StringField(validators=[DataRequired()])

    def validate_post_id(self, field):
        p = Post.query.get(field.data)

        if not p:
            raise ValidationError('Esta publicación no existe')
