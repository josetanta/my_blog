import os

from flask import Flask
from flask.testing import FlaskClient
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

from config import BASE_DIR, config
from .utils import format_urls

db = SQLAlchemy()
migrate = Migrate()
app_cors = CORS()


class CientAPITest(FlaskClient):
    def __init__(self, *args, **kwargs):
        try:
            self._authentication = kwargs.pop('authentication')
        except KeyError:
            self._authentication = None
        super(CientAPITest, self).__init__(*args, **kwargs)


def create_app(config_name):
    app = Flask(
        __name__,
        instance_relative_config=True,
        static_folder=os.path.join(BASE_DIR, 'static')
    )
    app.config.from_object(config.get(config_name))
    config[config_name].init_app(app)

    db.init_app(app)
    migrate.init_app(app, db)
    app_cors.init_app(app, resources={r"/api/*": {"origins": format_urls(os.getenv('CLIENT_CONCUSMER_URL'))}})

    from .api import api
    app.register_blueprint(api)

    # Client API Custom
    app.test_client_class = CientAPITest

    return app
