#!/bin/bash
export COMPOSE_FILE=local.yml
docker compose up -d --build

container_id=$(docker ps -aqf "name=flask")
docker rm -f $container_id

docker compose run --rm --name flask_blog_app_local --service-ports flask
