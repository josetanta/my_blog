from app.models import (
    User,
    Post,
    Role,
    Follow,
    Comment,
    UserToken,
    Account
)

from app import create_app, db
from flask_migrate import upgrade, migrate
import os
import click

from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path, verbose=True)


app = create_app(os.getenv('FLASK_ENV') or 'default')


@app.shell_context_processor
def shell_context():
    return dict(
        app=app,
        db=db,
        User=User,
        Post=Post,
        Role=Role,
        Follow=Follow,
        Comment=Comment,
        UserToken=UserToken,
        Account=Account
    )


@app.cli.command()
@click.argument('test_names', nargs=-1)
def test(test_names):
    """
    Running Test
    """
    import unittest

    if test_names:
        tests = unittest.TestLoader().loadTestsFromNames(test_names)
    else:
        tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command()
@click.option('--length', default=25,
              help='Number of functions to include in the profiler report.')
@click.option('--profile-dir', default=None,
              help='Directory where profiler data files are saved.')
def profile(length, profile_dir):
    """Start the application under the code profiler."""
    from werkzeug.middleware.profiler import ProfilerMiddleware
    from werkzeug.middleware.proxy_fix import ProxyFix
    app.wsgi_app = ProfilerMiddleware(
        app.wsgi_app, restrictions=[length],
        profile_dir=profile_dir
    )
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.run()


@app.cli.command("deploy")
def deploy():
    """Run deployment tasks."""
    # migrate database to latest revision
    migrate()
    upgrade()
    Role.generate_roles()
    db.session.commit()
