import os

BASE_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'app')

BD_CONNECTION = os.getenv('DB_CONNECTION', 'sqlite')
BD_USER = os.getenv('DB_USER', 'myblog')
BD_PASSWORD = os.getenv('DB_PASSWORD', 'myblog')
BD_HOST = os.getenv('DB_HOST', '')
BD_DATABASE = os.getenv('DB_DATABASE', 'myblogbd')


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY') or 'secret'
    WTF_CSRF_SECRET_KEY = os.getenv('SECRET_KEY') or 'secret'
    WTF_CSRF_ENABLED = False
    CORS_SEND_WILDCARD = "*"
    CORS_ORIGINS = "*"

    FLASK_DB_QUERY_TIMEOUT = 0.5
    APP_PER_PAGE = 10
    FLASK_SLOW_DB_QUERY_TIME = 0.5

    SSL_REDIRECT = False

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True

    EXPIRE_TOKEN = 2  # Expires in days
    MAX_AGE_TOKEN = 2 * 24 * 60 * 60  # 2 days
    EMAIL_BLOG_ADMIN = os.environ.get('MAIL_ADMIN')
    API_KEY = os.getenv('API_KEY_MAILGUN')
    API_URL = os.getenv('API_URL_MAILGUN')

    @staticmethod
    def connection(conn, name_bd='dev', default='sqlite'):
        connections: dict = {
            'sqlite': f'sqlite:///db/site-{name_bd}.sqlite3',
            'mysql': f'mysql+pymysql://{BD_USER}:{BD_PASSWORD}@{BD_HOST}/{BD_DATABASE}',
            'psql': f'postgresql://{BD_USER}:{BD_PASSWORD}@{BD_HOST}/{BD_DATABASE}',
        }
        return connections.get(conn) if conn else connections.get(default)

    @staticmethod
    def init_app(app):
        pass


class Development(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = Config.connection(BD_CONNECTION)


class Production(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = Config.connection(BD_CONNECTION, name_bd='production')

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)


class HerokuConfig(Production):
    SSL_REDIRECT = True if os.environ.get('DYNO') else False

    @classmethod
    def init_app(cls, app):
        Production.init_app(app)

        import logging
        from logging import StreamHandler
        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)


class TestApp(Config):
    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = Config.connection('sqlite', name_bd='test')
    SECRET_KEY = 'ff8561a4ed2d1833c861c715fa9d05'
    MAX_AGE_TOKEN = 5 * 60  # 5 sec


config = {
    'development': Development,
    'production': Production,
    'testing': TestApp,
    'heroku': HerokuConfig,

    'default': Development,
}
